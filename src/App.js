import React, { Component } from "react";
import { HashRouter as Router, Route, Link, Redirect } from "react-router-dom";
import Groups from "./components/groups/Main";
import Gallery from "./components/gallery/Main";
import Overview from "./components/overview/Main";
class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route
            exact
            path="/"
            render={() => {
              return (
                <div>
                  <Redirect to="/groups" />
                </div>
              );
            }}
          />
          <Route
            exact
            path="/groups"
            render={() => {
              return <Groups />;
            }}
          />
          <Route
            exact
            path="/gallery"
            render={props => {
              if (props.location.state) {
                const { groupId } = props.location.state;
                return <Gallery groupId={groupId} />;
              } else {
                return <Redirect to="/groups" />;
              }
            }}
          />
          <Route
            exact
            path="/overview"
            render={props => {
              if (props.location.state) {
                const { photos } = props.location.state;
                return <Overview photos={photos} />;
              } else {
                return <Redirect to="/groups" />;
              }
            }}
          />
        </div>
      </Router>
    );
  }
}

export default App;
