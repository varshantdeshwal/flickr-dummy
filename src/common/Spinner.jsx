import React from "react";
import ReactLoading from "react-loading";

const Spinner = props => {
  return (
    <div className="spinner">
      <ReactLoading type="spokes" color="rgb(43, 73, 136)" />
    </div>
  );
};

export default Spinner;
