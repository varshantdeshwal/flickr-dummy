import React, { Component } from "react";
import getPhotoInfo from "../../api-calls/getPhotoInfo";
import Highcharts from "highcharts";
class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sortedViews: [],
      sortedComments: [],
      othersViews: 0,
      othersComments: 0
    };
  }
  componentDidMount() {
    // getPhotoInfo(this.props.id).then(res => {
    //   this.setState({ name: res.photo.title._content, y: res.photo.views });
    // });
    this.getPhotosData()
      .then(photosData => {
        return {
          sortedViews: photosData.views.sort((a, b) => b.y - a.y),
          sortedComments: photosData.comments.sort((a, b) => b.y - a.y)
        };
      })
      .then(data => {
        this.setState({
          sortedViews: data.sortedViews,
          sortedComments: data.sortedComments
        });
        let len = data.sortedViews.length;
        if (len > 10) {
          this.getOthers(data).then(othersData =>
            this.setState({
              othersViews: othersData.othersViews,
              othersComments: othersData.othersComments
            })
          );
        }
      });
  }
  getOthers = async data => {
    let othersViews = 0;
    let othersComments = 0;
    await data.sortedViews.slice(10).map(d => {
      othersViews += d.y;
    });
    await data.sortedComments.slice(10).map(d => {
      othersComments += d.y;
    });
    return { othersViews: othersViews, othersComments: othersComments };
  };
  getPhotosData = async () => {
    let viewsArray = [];
    let commentsArray = [];
    await Promise.all(
      this.props.photos.map(
        async photo =>
          //   if (photo.id !== this.props.id) {
          await getPhotoInfo(photo.id).then(res => {
            viewsArray.push({
              name: res.photo.title._content,
              y: parseInt(res.photo.views)
            });
            commentsArray.push({
              name: res.photo.title._content,
              y: parseInt(res.photo.comments._content)
            });
          })
        //   }
      )
    );

    return {
      views: viewsArray,
      comments: commentsArray
    };
  };
  componentDidUpdate() {
    if (this.state.sortedViews.length > 10 && this.state.othersViews != 0) {
      this.chart(
        this.state.sortedViews.length > 10
          ? this.state.sortedViews
              .slice(0, 10)
              .concat({ name: "Others", y: this.state.othersViews })
          : this.state.sortedViews,
        "views",
        "views"
      );
      this.chart(
        this.state.sortedComments.length > 10
          ? this.state.sortedComments
              .slice(0, 10)
              .concat({ name: "Others", y: this.state.othersComments })
          : this.state.sortedComments,
        "comments",
        "comments"
      );
    }
  }
  chart = (chartData, id, type) =>
    Highcharts.chart(id, {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: "pie"
      },
      title: {
        text: "Top photos based on the " + type
      },
      tooltip: {
        pointFormat: "{series.name}: <b>{point.y:.1f}</b>"
      },
      credits: {
        enabled: false
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: "pointer",
          dataLabels: {
            enabled: true,
            format: "<b>{point.name}</b>: {point.percentage:.1f} %",
            style: {
              color:
                (Highcharts.theme && Highcharts.theme.contrastTextColor) ||
                "black"
            }
          }
        }
      },
      series: [
        {
          name: "Brands",
          colorByPoint: true,
          data: chartData
        }
      ]
    });
  render() {
    return (
      <div>
        <div id="views" className="pie-chart" />
        <div id="comments" className="pie-chart" />
      </div>
    );
  }
}
export default Main;
