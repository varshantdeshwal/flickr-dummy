import React, { Component } from "react";

import Autosuggest from "react-autosuggest";
class SearchBox extends Component {
  render() {
    const inputProps = {
      placeholder: "Search groups...",
      value: this.props.value,
      onChange: (e, v) => this.props.onChange(e, v),
      onBlur: (e, v) => {}
    };

    return (
      <Autosuggest
        suggestions={this.props.suggestions}
        onSuggestionsFetchRequested={this.props.onSuggestionsFetchRequested}
        onSuggestionsClearRequested={this.props.onSuggestionsClearRequested}
        getSuggestionValue={this.props.getSuggestionValue}
        renderSuggestion={this.props.renderSuggestion}
        inputProps={inputProps}
        onSuggestionSelected={(e, v) => this.props.onSuggestionSelected(e, v)}
      />
    );
  }
}
export default SearchBox;
