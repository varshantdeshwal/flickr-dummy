import React, { Component } from "react";
import SearchBox from "./SearchBox";
class SearchContainer extends Component {
  render() {
    const status = this.props.isLoading ? "Loading..." : "brghje";
    return (
      <div className="search-panel">
        <span className="search-box">
          <SearchBox
            onChange={(e, v) => this.props.onChange(e, v)}
            suggestions={this.props.suggestions}
            value={this.props.value}
            isLoading={this.props.isLoading}
            onSuggestionsFetchRequested={this.props.onSuggestionsFetchRequested}
            onSuggestionsClearRequested={this.props.onSuggestionsClearRequested}
            getSuggestionValue={this.props.getSuggestionValue}
            renderSuggestion={this.props.renderSuggestion}
            onSuggestionSelected={(e, v) =>
              this.props.onSuggestionSelected(e, v)
            }
          />
        </span>
        {/* <span className="status">
          <strong>{status}</strong>
        </span> */}
      </div>
    );
  }
}
export default SearchContainer;
