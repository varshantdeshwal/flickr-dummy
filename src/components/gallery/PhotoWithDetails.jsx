import React, { Component } from "react";
import getPhotoInfo from "../../api-calls/getPhotoInfo";
import LazyLoad from "react-lazy-load";

class PhotoWithDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      description: "",
      owner: "",
      views: "",
      comments: "",
      dateuploaded: ""
    };
  }
  componentDidMount() {
    getPhotoInfo(this.props.photoId).then(res => {
      this.setState({
        title: res.photo.title._content,
        description: res.photo.description._content,
        owner: res.photo.owner.realname,
        views: res.photo.views,
        comments: res.photo.comments._content,
        dateuploaded: res.photo.dateuploaded
      });
    });
  }
  render() {
    return (
      <div>
        <LazyLoad>
          <article>
            <img src={this.props.photoUrl} />
            <div className="photo-details">
              <h3>{this.state.title}</h3>
              <p>
                {this.state.description.length > 70
                  ? ""
                  : this.state.description}
              </p>
              <h4>
                By {this.state.owner === "" ? "Unknown" : this.state.owner}
              </h4>
              <div>
                <span>
                  <i class="fas fa-eye" />
                </span>
                <span>{this.state.views}</span>
                <span>
                  <i class="far fa-comments" />
                </span>
                <span>{this.state.comments}</span>
              </div>
            </div>
          </article>
        </LazyLoad>
      </div>
    );
  }
}
export default PhotoWithDetails;
