import React, { Component } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import getSomeGroupPhotos from "../../api-calls/getSomeGroupPhotos";
import { Link } from "react-router-dom";
import PhotoWithDetails from "./PhotoWithDetails";

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = { hasMore: true, page: 1, photos: [], redirect: false };
  }
  componentDidMount() {
    this.fetchPhotos(this.props.groupId, 20, this.state.page);
  }
  fetchPhotos = (groupId, perPage, page) => {
    getSomeGroupPhotos(groupId, perPage, page).then(res => {
      if (res.stat !== "fail") {
        if (this.state.page === res.photos.pages) {
          this.setState({
            photos: this.state.photos.concat(res.photos.photo),

            hasMore: false
          });
        } else {
          this.setState({
            photos: this.state.photos.concat(res.photos.photo),
            page: this.state.page + 1
          });
        }
      } else {
        this.setState({ hasMore: false });
      }
    });
  };
  fetchMorePhotos = () => {
    this.fetchPhotos(this.props.groupId, 20, this.state.page);
  };

  render() {
    return (
      <div className="gallery-conatiner">
        <InfiniteScroll
          dataLength={this.state.photos.length}
          next={this.fetchMorePhotos}
          hasMore={this.state.hasMore}
          loader={<h4>Loading...</h4>}
        >
          <div className="main">
            {this.state.photos.length !== 0 ? (
              this.state.photos.map(photo => (
                <Link
                  to={{
                    pathname: "/overview",
                    state: {
                      photos: this.state.photos
                    }
                  }}
                  className="link"
                  style={{ textDecoration: "none" }}
                >
                  <PhotoWithDetails
                    groupId={this.props.groupId}
                    photoId={photo.id}
                    photoUrl={
                      "https://farm" +
                      photo.farm +
                      ".staticflickr.com/" +
                      photo.server +
                      "/" +
                      photo.id +
                      "_" +
                      photo.secret +
                      "_n.jpg"
                    }
                  />
                </Link>
              ))
            ) : (
              <div>Photos not available</div>
            )}
          </div>
        </InfiniteScroll>
      </div>
    );
  }
}
export default Main;
