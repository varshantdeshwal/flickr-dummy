import React, { Component } from "react";

import getGroups from "../../api-calls/getGroups";
import { debounce } from "throttle-debounce";
import GroupCard from "./GroupCard";
import InfiniteScroll from "react-infinite-scroll-component";
import SearchContainer from "../search-bar/SearchContainer";
import Spinner from "../../common/Spinner";
import { Link } from "react-router-dom";
import Chart from "./Chart";

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: "",
      groups: [],
      page: 1,
      totalPages: 0,
      currentFetched: [],
      hasMore: false,
      spinner: false,
      value: "",
      suggestions: [],
      isLoading: false,
      refreshChart: false,
      message: "Explore flickr groups here !"
    };
    this.getOptions = debounce(500, this.getOptions);
    this.debouncedLoadSuggestions = debounce(1000, this.loadSuggestions);
  }

  componentDidMount() {
    this.setState({ page: 1 });
  }
  filterData = async data => {
    var requiredData = [];
    await data.map(item =>
      requiredData.push({
        id: item.nsid,
        name: item.name
      })
    );
    return requiredData;
  };

  fetchMoreGroups = () => {
    if (this.state.page === this.state.totalPages) {
      this.setState({ hasMore: false });
    } else {
      getGroups(this.state.value, this.state.page + 1).then(res => {
        this.setState({
          groups: this.state.groups.concat(res.groups.group),
          page: this.state.page + 1,
          refreshChart: true,
          currentFetched: res.groups.group
        });
      });
    }
  };

  loadSuggestions(value) {
    this.setState({
      isLoading: true,
      spinner: true
    });

    getGroups(value, this.state.page).then(res => {
      if (res.groups.group.length === 0) {
        this.setState({ message: "No groups found :(" });
      }
      this.filterData(res.groups.group).then(data => {
        this.setState({
          groups: res.groups.group,
          refreshChart: false,
          totalPages: res.groups.total,
          page: 1,
          hasMore: true,
          currentFetched: res.groups.group,
          spinner: false,
          isLoading: false,
          suggestions: data
        });
      });
    });
  }
  getSuggestionValue = suggestion => {
    return suggestion.name;
  };

  renderSuggestion = suggestion => {
    return <span>{suggestion.name}</span>;
  };
  onChange = (event, { newValue }) => {
    this.setState({
      value: newValue
    });
  };
  shouldRenderSuggestions = value => {
    return this.state.value === value;
  };

  onSuggestionsFetchRequested = ({ value, reason }) => {
    if (reason === "input-changed") {
      this.debouncedLoadSuggestions(value);
    } else {
      return;
    }
  };
  undoRefresh = () => {
    this.setState({ refreshChart: false });
  };
  onSuggestionsClearRequested = () => {
    this.setState({});
  };
  onSuggestionSelected = (e, v) => {
    this.setState({
      refreshChart: true,
      groups: [this.state.groups[v.suggestionIndex]],
      currentFetched: [this.state.groups[v.suggestionIndex]],
      hasMore: false
    });
  };
  render() {
    return (
      <div className="group-main-conatiner">
        <SearchContainer
          onChange={(e, v) => this.onChange(e, v)}
          suggestions={this.state.suggestions}
          value={this.state.value}
          isLoading={this.state.isLoading}
          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          getSuggestionValue={this.getSuggestionValue}
          renderSuggestion={this.renderSuggestion}
          shouldRenderSuggestions={this.shouldRenderSuggestions}
          onSuggestionSelected={(e, v) => this.onSuggestionSelected(e, v)}
        />

        <Chart
          currentTotalLength={this.state.groups.length}
          groups={this.state.currentFetched}
          shouldRefresh={this.state.refreshChart}
          allGroups={this.state.groups}
        />

        <InfiniteScroll
          dataLength={this.state.groups !== null ? this.state.groups.length : 0}
          next={this.fetchMoreGroups}
          hasMore={this.state.hasMore}
          loader={<h4>Loading...</h4>}
        >
          <div className="group-results-container">
            {this.state.spinner ? <Spinner /> : null}
            {this.state.groups.length !== 0 ? (
              this.state.groups.map(group => (
                <Link
                  to={{
                    pathname: "/gallery",
                    state: { groupId: group.nsid }
                  }}
                  className="link"
                  style={{ textDecoration: "none" }}
                >
                  <GroupCard group={group} />
                </Link>
              ))
            ) : (
              <div className="groups-main-explore">{this.state.message}</div>
            )}
          </div>
        </InfiniteScroll>
      </div>
    );
  }
}
export default Main;
