import React from "react";

const GroupName = props => {
  return (
    <div className="group-card-item-container">
      <h4>{props.name}</h4>
    </div>
  );
};
export default GroupName;
