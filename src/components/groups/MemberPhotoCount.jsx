import React, { Suspense } from "react";

const MemberPhotoCount = props => {
  return (
    <div className="group-card-item-container">
      <span className="count-icon">
        <i class="fas fa-users " />
      </span>
      <span className="count">{props.memberCount}</span>
      <span className="col-sm-1" />
      <span className="count-icon">
        <i class="fas fa-images " />
      </span>
      <span className="count">{props.photoCount}</span>
    </div>
  );
};
export default MemberPhotoCount;
