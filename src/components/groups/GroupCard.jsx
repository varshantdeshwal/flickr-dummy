import React, { Component, lazy, Suspense } from "react";
import GroupIcon from "./GroupIcon";
import MemberPhotoCount from "./MemberPhotoCount";
import GroupName from "./GroupName";
import GroupPhotos from "./GroupPhotos";
// const GroupIcon = lazy(() => import("./GroupIcon"));
class GroupCard extends Component {
  render() {
    return (
      <div className="group-card-main">
        {/* <Suspense fallback={<div>Loading...</div>}> */}
        <GroupIcon
          url={
            "http://farm" +
            this.props.group.iconfarm +
            ".staticflickr.com/" +
            this.props.group.iconserver +
            "/buddyicons/" +
            this.props.group.nsid +
            ".jpg"
          }
        />
        <GroupName name={this.props.group.name} />
        <MemberPhotoCount
          memberCount={this.props.group.members}
          photoCount={this.props.group.pool_count}
        />

        {/* </Suspense> */}
        <GroupPhotos groupId={this.props.group.nsid} />
      </div>
    );
  }
}
export default GroupCard;
