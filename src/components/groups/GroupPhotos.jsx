import React, { Component } from "react";
import getSomeGroupPhotos from "../../api-calls/getSomeGroupPhotos";
import Photo from "./Photo";
class GroupPhotos extends Component {
  constructor(props) {
    super(props);
    this.state = { photos: [], groupId: "" };
  }
  componentDidMount() {
    this.fetchPhotos(this.props.groupId);
  }

  componentWillReceiveProps(props) {
    this.fetchPhotos(props.groupId);
  }
  fetchPhotos = groupId => {
    getSomeGroupPhotos(groupId, 6, 1).then(res => {
      if (res.stat !== "fail") {
        this.setState({ photos: res.photos.photo });
      } else {
        this.setState({ photos: [] });
      }
    });
  };

  render() {
    return (
      <div className="group-card-photos-container">
        {this.state.photos !== null ? (
          this.state.photos.length !== 0 ? (
            this.state.photos.map(photo => (
              <Photo
                url={
                  "https://farm" +
                  photo.farm +
                  ".staticflickr.com/" +
                  photo.server +
                  "/" +
                  photo.id +
                  "_" +
                  photo.secret +
                  "_q.jpg"
                }
              />
            ))
          ) : (
            <div className="no-group-photos">Photos not available :(</div>
          )
        ) : null}
      </div>
    );
  }
}
export default GroupPhotos;
