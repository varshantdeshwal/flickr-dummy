import React, { Suspense } from "react";

import LazyLoad from "react-lazy-load";
const Photo = props => {
  return (
    <div className="group-photo">
      <LazyLoad>
        <img src={props.url} />
      </LazyLoad>
    </div>
  );
};
export default Photo;
