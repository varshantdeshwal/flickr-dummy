import React, { Suspense } from "react";

import LazyLoad from "react-lazy-load";
const GroupIcon = props => {
  return (
    <div className="group-icon-container">
      <LazyLoad>
        <img src={props.url} className="group-icon" />
      </LazyLoad>
    </div>
  );
};
export default GroupIcon;
