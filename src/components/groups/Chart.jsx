import React, { Component } from "react";
import Highcharts from "highcharts";
class Chart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shouldIpdate: true,
      names: [],
      photos: [],
      allGroups: [],
      ref: ""
    };
  }
  createChartData = async groups => {
    let groupNamesArray = [];
    let groupPhotosCountArray = [];
    await groups.map(group => {
      groupNamesArray.push(group.name);
      groupPhotosCountArray.push(parseInt(group.pool_count));
    });
    return {
      names: groupNamesArray,
      photos: groupPhotosCountArray,
      allGroups: []
    };
  };

  static getDerivedStateFromProps(props, prevState) {
    if (props.allGroups.length !== 0) {
      if (props.shouldRefresh === true) {
        if (prevState.names.length < props.currentTotalLength) {
          return { ref: null };
        } else return null;
      } else {
        if (prevState.allGroups.length === 0) {
          return {
            ref: "new"
          };
        } else {
          if (prevState.allGroups[0].nsid !== props.allGroups[0].nsid) {
            return {
              ref: "changed"
            };
          } else return null;
        }
      }
    }
  }

  componentDidUpdate() {
    if (this.state.ref === null) {
      this.createChartData(this.props.groups).then(result => {
        this.setState({
          names: this.state.names.concat(result.names),
          photos: this.state.photos.concat(result.photos),
          ref: "",
          allGroups: this.props.allGroups
        });
      });
    } else if (this.state.ref === "changed" || this.state.ref === "new") {
      this.createChartData(this.props.groups).then(result => {
        this.setState({
          names: result.names,
          photos: result.photos,
          ref: "",
          allGroups: this.props.allGroups
        });
      });
    }
    if (this.props.allGroups.length !== 0) this.chart();
  }

  chart = () =>
    Highcharts.chart("photo-count-chart", {
      chart: {
        type: "column",
        marginTop: 30,
        marginBottom: 50
      },
      legend: { verticalAlign: "top", align: "right" },
      title: {
        text: "Group Photos Count "
      },
      subtitle: {
        align: "right",
        text: "Source: Flickr.com"
      },
      xAxis: {
        categories: this.state.names,
        crosshair: true
      },
      yAxis: {
        min: 0,
        title: {
          text: "Photos Count"
        }
      },
      tooltip: {
        headerFormat:
          '<span style="font-size:15px;color:{series.color}">{point.key}</span><table>',
        pointFormat:
          '<tr> <td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
        footerFormat: "</table>",

        shared: true,
        useHTML: true
      },
      credits: {
        enabled: false
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0
        }
      },
      series: [
        {
          name: "Group",
          data: this.state.photos
        }
      ]
    });
  render() {
    return this.props.allGroups.length !== 0 ? (
      <div id="photo-count-chart" className="photo-count-chart" />
    ) : (
      <div id="photo-count-chart" />
    );
  }
}
export default Chart;
