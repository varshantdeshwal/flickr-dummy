import api_key from "../common/key";
var result = async function getSomeGroupPhotos(groupId, perPage, page) {
  const res = await fetch(
    "https://api.flickr.com/services/rest/?method=flickr.groups.pools.getPhotos&api_key=" +
      api_key +
      "&group_id=" +
      groupId +
      "&per_page=" +
      perPage +
      "&page=" +
      page +
      "&format=json&nojsoncallback=1",

    {
      method: "GET"
    }
  );
  const data = await res.json();

  return data;
};

export default result;
