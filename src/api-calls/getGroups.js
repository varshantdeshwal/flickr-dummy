import api_key from "../common/key";

var result = async function getgroups(keyword, page) {
  const res = await fetch(
    "https://api.flickr.com/services/rest/?method=flickr.groups.search&api_key=" +
      api_key +
      "&text=" +
      keyword +
      "&per_page=20&page=" +
      page +
      "&format=json&nojsoncallback=1",

    {
      method: "GET"
    }
  );
  const data = await res.json();

  return data;
};

export default result;
