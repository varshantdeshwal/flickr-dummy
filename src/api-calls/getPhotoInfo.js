import api_key from "../common/key";

https: var result = async function getPhotoInfo(photoId) {
  const res = await fetch(
    "https://api.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key=" +
      api_key +
      "&photo_id=" +
      photoId +
      "&format=json&nojsoncallback=1",

    {
      method: "GET"
    }
  );
  const data = await res.json();

  return data;
};

export default result;
